require "../BusinessClasses/Good"
require "../BusinessClasses/TaxedGood"
require "../BusinessClasses/ImportedTaxedGood"

require 'minitest/autorun'

class ImportedTaxedGoodTests < MiniTest::Unit::TestCase
	def tests

		assert_equal(1000, ImportedTaxedGood.new(1,1000).cost)
		assert_equal(100, ImportedTaxedGood.new(1,1000).basic_tax(1000))
		assert_equal(50, ImportedTaxedGood.new(1,1000).import_duty(1000))


	end
end