        require "../BusinessClasses/Good"
        require "../BusinessClasses/TaxedGood"
        require"../BusinessClasses/ExemptedGood"
        require "../BusinessClasses/ImportedExemptedGood"
        require "../BusinessClasses/ImportedTaxedGood"
        
        
        
        begin
            #prompt user for input
            print "Item Name:"
            item = gets.chomp
            print "Quantity:"
            qty = gets.chomp
            quantity = qty.to_i
            print "enter unit of measure like kg,packet etc :"
            unit = gets.chomp
            print "price:"
            pri = gets.chomp
            price = pri.to_f
            print "please enter 1 for taxed good and 0 for book/food/medic product :" 
            gd = gets.chomp
            goodtype = gd.to_i
            print "please enter 1 for an imported good and 0 otherwise :"
            imp = gets.chomp
            importedgood = imp.to_i

            #ask if user should continue adding items to cart.
            print "Do you want to add more items to the chart ? enter 1 if yes and 0 otherwise :"
            cont = gets
            continue = cont.to_i
            

            if goodtype == 1 && importedgood != 1
                cost = TaxedGood.new(quantity,price).cost
                salestax = (TaxedGood.new(quantity,price).basic_tax(cost)).round(2)
                tx = [cost]
                sx = [salestax]
                basket = ["#{quantity}  #{unit}  of #{item} at #{cost}"]
               
            elsif goodtype != 1 && importedgood != 1
                cost = ExemptedGood.new(quantity,price).cost
                salestax = (ExemptedGood.new(quantity,price).basic_tax(cost).round(2))
                tx = [cost]
                sx = [salestax]
                basket = ["#{quantity}  #{unit}  of #{item} at #{cost} "]
                
               
            elsif goodtype == 1 && importedgood == 1
                cost = ImportedTaxedGood.new(quantity,price).cost
                basictax = (ImportedTaxedGood.new(quantity,price).basic_tax(cost)).round(2)
                importduty = (ImportedTaxedGood.new(quantity,price).import_duty(cost)).round(2)
                salestax = (basictax + importduty).round(2)
                tx = [cost]
                sx = [salestax]
                basket = ["#{quantity} imported #{unit}  of #{item} at #{cost} "]
                
            elsif goodtype != 1 && importedgood == 1 
                cost = ImportedExemptedGood.new(quantity,price).cost
                basictax = (ImportedExemptedGood.new(quantity,price).basic_tax(cost)).round(2)
                importduty = (ImportedExemptedGood.new(quantity,price).import_duty(cost)).round(2)
                salestax = (basictax + importduty).round(2)
                tx = [cost]
                sx = [salestax]
                basket = ["#{quantity} imported #{unit}  of #{item} at #{cost} "]
               
            end

            break if continue == "exit"
            
            #container for the items being bought
            cart = []
            #appending what has been purchased to the container
            cart << basket

            #container to store cost for each item that has been bought
            totalcost = []
            totalcost << tx

            #container to store tax on each item that has been bought
            totalsalestax = []
            totalsalestax << sx

            #append the contents of the container for cost in a file
            costFile = File.open('cost.txt', 'a')   
            costFile . puts totalcost
            costFile.close

            #append the contents of the container for tax in a file
            taxFile = File.open('tax.txt', 'a')   
            taxFile . puts totalsalestax
            taxFile.close

            #append the contents of the container for items in a file
            itemsFile = File.open('data.txt', 'a')   
            itemsFile . puts cart
            itemsFile.close
             
            
        end while continue == 1

        # retrieving content from the items,cost and tax files respectively.
        itemsFileContents = open('data.txt','r').map { |line| line.split("\n")[0] }
        costFileContents = open('cost.txt','r').map { |line| line.split("\n")[0] }
        taxFileContents = open('tax.txt','r').map { |line| line.split("\n")[0] }

        #coverting the returned arrays into float for the cost and tax containers.
        convertedcostFileContents = costFileContents.collect{|i| i.to_f}
        convertedtaxFileContents = taxFileContents.collect{|x| x.to_f}

        #printing reciept
        p "-----------------------------"
        itemsFileContents.each do |contents|
            p contents
                
        end
        
        p "totalcost: #{(convertedcostFileContents.reduce :+).round(2)}"
        p "salestax: #{(convertedtaxFileContents.reduce :+).round(2)}"

        p "-----------------------------"
            
        #deleting the contents of the files for the next transaction.
        itemsFileContents = open("data.txt", "w")
        itemsFileContents.close

        costFileContents = open("cost.txt", "w")
        costFileContents.close

        taxFileContents = open("tax.txt", "w")
        taxFileContents.close
            
        
        



        
        
