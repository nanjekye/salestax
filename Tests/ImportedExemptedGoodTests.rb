require "../BusinessClasses/Good"
require "../BusinessClasses/ExemptedGood"
require "../BusinessClasses/ImportedExemptedGood"

require 'minitest/autorun'


class ImportedExemptedGoodTests < MiniTest::Unit::TestCase
	def tests

		assert_equal(1000, ImportedExemptedGood.new(1,1000).cost)
		assert_equal(0, ImportedExemptedGood.new(1,1000).basic_tax(1000))
		assert_equal(50, ImportedExemptedGood.new(1,1000).import_duty(1000))

	end
end