
class TaxedGood < Good
	TaxRate = (10.00/100)
	def initialize(quantity,price)
		super(quantity,price)
			
	end

	#basic tax on a product
	def basic_tax(productCost)

		tax = TaxRate * productCost
		return tax.round(2)
	end

end