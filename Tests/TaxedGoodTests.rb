
require "../BusinessClasses/Good"
require "../BusinessClasses/TaxedGood"

require 'minitest/autorun'
class TaxedGoodTests < MiniTest::Unit::TestCase
	def tests

		assert_equal(1000, TaxedGood.new(1,1000).cost)
		assert_equal(100, TaxedGood.new(1,1000).basic_tax(1000))

	end
end