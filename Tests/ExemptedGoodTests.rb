require "../BusinessClasses/Good"
require "../BusinessClasses/ExemptedGood"


require 'minitest/autorun'


class ExemptedGoodTests < MiniTest::Unit::TestCase
	def tests

		assert_equal(1000, ExemptedGood.new(1,1000).cost)
		assert_equal(0, ExemptedGood.new(1,1000).basic_tax(1000))

	end
end